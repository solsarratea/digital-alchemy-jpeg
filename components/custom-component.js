const React = require('react')

function saveImage(e,updateImagePath) {
    const imgPath = document.querySelector('input[type=file]').files[0];
    const reader = new FileReader();

    reader.addEventListener("load", function () {
        // convert image file to base64 string and save to localStorage
        localStorage.setItem("image", reader.result);
    }, false);

    if (imgPath) {
        reader.readAsDataURL(imgPath);
    }

    location.reload();

}

class CustomComponent extends React.Component {

    render() {

        const { hasError, idyll, updateProps, id, onSuccess, ...props } = this.props;

        return (<div style={{}}>
                <label className={"btn"} htmlFor={id}>
                <input
                id={id}
                name="photo"
                type={"file"}
                onChange={(e)=>{ saveImage(e); onSuccess()}}
                />
                </label>
                </div>
               );
    }
}

module.exports = CustomComponent;
